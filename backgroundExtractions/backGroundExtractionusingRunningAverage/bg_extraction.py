#
# trzy frame, które różnie pamiętają w stecz jaki był obraz
#
# http://opencvpython.blogspot.com/2012/07/background-extraction-using-running.html
import cv2
import numpy as np

c = cv2.VideoCapture(0)
while c.isOpened():
    ret, f = c.read()
    if ret:
        avg1 = np.float32(f)
        avg2 = np.float32(f)
        avg3 = np.float32(f)
        break
kernel = np.ones((5, 5), np.uint8)
learning = 1
while 1:
    ret, f = c.read()
    # if(learning<40):
    cv2.accumulateWeighted(f, avg1, 0.1)
    cv2.accumulateWeighted(f, avg2, 0.05)
    cv2.accumulateWeighted(f, avg3, 0.01)
    learning = learning + 1

    res1 = cv2.convertScaleAbs(avg1)
    res2 = cv2.convertScaleAbs(avg2)
    res3 = cv2.convertScaleAbs(avg3)

    cv2.imshow('img', f)
    cv2.imshow('avg1', res1)
    cv2.imshow('avg2', res2)
    cv2.imshow('avg3', res3)

    cv2.imshow('sub1', cv2.dilate(cv2.erode(cv2.threshold(cv2.cvtColor(f - res1, cv2.COLOR_RGB2GRAY), 0, 255,
                                                          cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1], kernel), kernel))
    cv2.imshow('sub2', cv2.dilate(cv2.erode(cv2.threshold(cv2.cvtColor(f - res2, cv2.COLOR_RGB2GRAY), 0, 255,
                                                          cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1], kernel), kernel))
    cv2.imshow('sub3', cv2.dilate(cv2.erode(cv2.threshold(cv2.cvtColor(f - res3, cv2.COLOR_RGB2GRAY), 0, 255,
                                                          cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1], kernel), kernel))
    k = cv2.waitKey(20)

    if k == ord('q'):
        break

cv2.destroyAllWindows()
c.release()
