# https://github.com/jrosebr1/imutils
# USAGE
# python motion_detector.py

# import the necessary packages
import argparse
import datetime
import math

import cv2
import imutils
import numpy as np
from PIL import Image
from PIL import ImageChops
from scipy import sum
from scipy.linalg import norm


def rmsdiff_2011(im1, im2):
    "Calculate the root-mean-square difference between two images"
    diff = ImageChops.difference(im1, im2)
    h = diff.histogram()
    sq = (value*(idx**2) for idx, value in enumerate(h))
    sum_of_squares = sum(sq)
    rms = math.sqrt(sum_of_squares/float(im1.size[0] * im1.size[1]))
    return rms

# function for images comparament
def compare_images(img1, img2):
    # normalize to compensate for exposure difference, this may be unnecessary
    # consider disabling it
    #img1 = cv2.normalize(img1)
    #img2 = cv2.normalize(img2)
    # calculate the difference and its norms
    diff = img1 - img2  # elementwise for scipy arrays
    m_norm = sum(abs(diff))  # Manhattan norm
    z_norm = norm(diff.ravel(), 0)  # Zero norm
    return (m_norm, z_norm)

# helpful function to print out the differential values of the images
def print_differences(n_m, n_0, img1):
    print "Manhattan norm:", n_m, "/ per pixel:", n_m / img1.size
    print "Zero norm:", n_0, "/ per pixel:", n_0 * 1.0 / img1.size

cascPath = "haarcascade_mcs_eyepair_small.xml"
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml')

runN = False
delay = 50  # TODO setting frames to take for not movement
framesList = [delay]
# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-a", "--min-area", type=int, default=500, help="minimum area size")
args = vars(ap.parse_args())


# initialize the first frame in the video stream
firstFrame = None
camera = cv2.VideoCapture(0)
# loop over the frames of the video
while True:
    (grabbed, frame) = camera.read()
    if grabbed:
        if runN:
            # grab the current frame and initialize the occupied/unoccupied
            # text
            text = "Unoccupied"

            # resize the frame, convert it to grayscale, and blur it
            frame = imutils.resize(frame, width=500)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (21, 21), 0)

            grayForFaces = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)      # gray without blurring for faces
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)         # ffaces detection

            for (x,y,w,h) in faces:
                cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
                roi_gray = gray[y:y+h, x:x+w]
                roi_color = frame[y:y+h, x:x+w]
                eyes = eye_cascade.detectMultiScale(roi_gray)
                for (ex,ey,ew,eh) in eyes:
                    cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)

            # if the first frame is None, initialize it
            if firstFrame is None:
                firstFrame = gray
                continue

            # adding frames to the delay list
            if(len(framesList) < delay):                                    # adding frames to the delay queue
                framesList.append(gray)
            else:                                                       # if already filled with needed elements
                framesList.pop(0)                                           # remove first frame
                framesList.append(gray)                                    # and add actual frame at the end

            #for i in range(0,len(framesList)-1):
            #    cv2.imshow('takietam', cv2.absdiff(list(framesList)[0],list(framesList)[i+1]))

            if len(framesList) < delay:
                True
            else:
                for i in range(1, len(framesList)-1):           #TODO nie wiem dlaczego od 0 nie dziala

                    im1 = Image.fromarray(np.array(list(framesList)[i]))
                    im2 = Image.fromarray(np.array(list(framesList)[i+1]))
                    diff1 = ImageChops.difference(im1, im2)
                    print('Iteracja %d, %s i jakasDIFF %f' % (i, diff1.getbbox(), rmsdiff_2011(im1, im2)))

                    if rmsdiff_2011(im1, im2) <= 2 and i == len(framesList)-2:
                        firstFrame = gray
                        print '[INFO] FirstFrame changed!'

                    #im1 = cv2.absdiff(list(framesList)[i],list(framesList)[i+1])
                    #im2 = cv2.absdiff(list(framesList)[i+1],list(framesList)[i+2])
                    #n_m, n_0 = compare_images(cv2.absdiff(list(framesList)[i],list(framesList)[i+1]),cv2.absdiff(list(framesList)[i+1],list(framesList)[i+2]))

                    #n_m, n_0 = compare_images(list(framesList)[i], list(framesList)[i+1])       # comparing differences between every image
                    #print('Iteracja %d, %f, %f' % (i, n_m, n_0))            #TODO nie wiem dlaczego caly czas osiaga wysokie wartosci :( @up teraz tylko 0...
                    #if not n_m==0 or not n_0==0:
                    #    break
                    #elif i==len(framesList)-2:
                    #    firstFrame = gray
                    #    print '[INFO] FirstFrame changed!'
            #for i in range(0, len(framesList)-1):
                #frameDelta = cv2.absdiff(list(framesList)[0], list(framesList)[i+1])

            # compute the absolute difference between the current frame and
            # first frame
            frameDelta = cv2.absdiff(firstFrame, gray)
            thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

            # dilate the thresholded image to fill in holes, then find contours
            # on thresholded image
            thresh = cv2.dilate(thresh, None, iterations=2)
            (cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                         cv2.CHAIN_APPROX_SIMPLE)

            # loop over the contours
            for c in cnts:
                # if the contour is too small, ignore it
                if cv2.contourArea(c) < args["min_area"]:
                    continue

                # compute the bounding box for the contour, draw it on the frame,
                # and update the text
                (x, y, w, h) = cv2.boundingRect(c)
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                text = "Occupied"

            # draw the text and timestamp on the frame
            cv2.putText(frame, "Room Status: {}".format(text), (10, 20),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                        (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

            # show the frame and record if the user presses a key
            #cv2.imshow("Security Feed", frame)
            cv2.imshow("Thresh", thresh)
            cv2.imshow("Frame Delta", frameDelta)
        cv2.imshow("Security Feed", frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('m'):
        runN = True
    if key == ord("q"):
        break
# cleanup the camera and close any open windows
camera.release()
cv2.destroyAllWindows()

