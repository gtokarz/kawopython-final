import cv2
import numpy as np

cam = cv2.VideoCapture(0)
fgbg = cv2.BackgroundSubtractorMOG2()
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
runN = False
while(cam.isOpened()):
    ret, frame = cam.read()
    if ret:
        if runN:
            fgmask = fgbg.apply(frame)

            fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
            masked = cv2.bitwise_and(frame, frame, mask=fgmask)
            #cv2.imshow('fgmask', fgmask)


            gray = cv2.cvtColor(masked, cv2.COLOR_RGB2GRAY)
            circles = cv2.HoughCircles(gray, cv2.cv.CV_HOUGH_GRADIENT, 1, 10, param1=20, param2=90, minRadius=0, maxRadius=120)
            if circles is not None:
                circles = np.uint16(np.around(circles))
                for i in circles[0,:]:
                    # draw the outer circle
                    cv2.circle(masked, (i[0], i[1]), i[2], (0,255,0), 2)
                    # draw the center of the circle
                    cv2.circle(masked, (i[0], i[1]), 2, (0,0,255), 3)

            cv2.imshow('test', masked)
            cv2.imshow('Canny detection',cv2.Canny(frame,100,200))
        cv2.imshow('frame', frame)

    key = cv2.waitKey(1) & 0xFF
    if key == ord('m'):
        runN = True
    if key == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()
