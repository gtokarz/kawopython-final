#
# wycina kapsulki
#
# HOUGH http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_houghcircles/py_houghcircles.html
import copy

import cv2
import numpy as np

lista_wycietych_kapsulek = []
lista_wycietych_twarzy = []

face_cascade = cv2.CascadeClassifier('../haarcascades/haarcascade_frontalface_default.xml')
eye_cascade = cv2.CascadeClassifier('../haarcascades/haarcascade_eye.xml')


def wycinka_kapsulek(img):
    clear_img = copy.copy(img)
    # cv.Smooth(processed, processed, cv.CV_GAUSSIAN, 7, 7)
    blurred_img = cv2.medianBlur(img,
                                 5)  # EXPL blurring an image to improve circle detection - less circle detected! #INFO parameter has to be odd
    gray_img = cv2.cvtColor(blurred_img, cv2.COLOR_RGB2GRAY)

    circles = cv2.HoughCircles(gray_img, cv2.cv.CV_HOUGH_GRADIENT, 1, 10, param1=20, param2=90, minRadius=0,
                               maxRadius=120)
    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)  # TODO DEBUG
            # draw the center of the circle
            cv2.circle(img, (i[0], i[1]), 2, (0, 0, 255), 3)  # TODO DEBUG
            lista_wycietych_kapsulek.append(clear_img[(i[1] - i[2]):(i[1] + i[2]), (i[0] - i[2]):(i[0] + i[2])])
    if len(lista_wycietych_kapsulek) > 0:
        cv2.imshow('Last circle', lista_wycietych_kapsulek[len(lista_wycietych_kapsulek) - 1])
        # cv2.imshow('Imput image', clear_img)


def wycinka_twarzy(img):
    clear_img = copy.copy(img)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray_img, 1.3, 5)

    if faces is not None:
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)  # TODO DEBUG
            roi_gray = gray_img[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]
            eyes = eye_cascade.detectMultiScale(roi_gray)
            for (ex, ey, ew, eh) in eyes:
                cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 0, 255), 2)  # TODO DEBUG

            lista_wycietych_twarzy.append(clear_img[y:(y + h), x:(x + w)])  # dodanie do listy
    if len(lista_wycietych_twarzy) > 0:
        cv2.imshow('Last face', lista_wycietych_twarzy[len(lista_wycietych_twarzy) - 1])
        # Display the resulting frame
        # cv2.imshow('Input image for faces', img)


if __name__ == '__main__':
    kamera = cv2.VideoCapture(0)

    while kamera.isOpened():
        ret, img = kamera.read()
        if ret:
            wycinka_kapsulek(img)
            wycinka_twarzy(img)

            key = cv2.waitKey(1) & 0xFF
            if key == ord('q'):
                break

        cv2.imshow('Input image', img)

    kamera.release()
    cv2.destroyAllWindows()
