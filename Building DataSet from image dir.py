import os

import graphlab as gl

dnn = gl.load_model('dnn.gl')
MS_coffe_sf = gl.SFrame({'image': gl.SArray(os.listdir('coffe_logger_data/recorder/MS_coffe/')).apply(lambda x: gl.image_analysis.resize(gl.Image('coffe_logger_data/recorder/MS_coffe/'+x),256,256,3))})

#prepare none set
MS_coffe_sf['face'] = gl.SArray(range(len(MS_coffe_sf['image']))).apply(lambda x: 'T')
MS_coffe_sf['coffe'] = gl.SArray(range(len(MS_coffe_sf['image']))).apply(lambda x: 'T')
MS_coffe_sf['person'] = gl.SArray(range(len(MS_coffe_sf['image']))).apply(lambda x: 'MS')
MS_coffe_sf['coffe_type'] = gl.SArray(range(len(MS_coffe_sf['image']))).apply(lambda x: 'GRANDE')
MS_coffe_sf['deep_features'] = dnn.extract_features(MS_coffe_sf)

MS_coffe_sf.save('coffe_logger_data/recorder/MS_coffe/sf')