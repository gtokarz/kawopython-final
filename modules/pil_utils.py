import StringIO as _StringIO

import PIL
import graphlab as gl

_format = {'JPG': 0, 'PNG': 1, 'RAW': 2, 'UNDEFINED': 3}

def from_pil_image(pil_img):
    """
    Returns a graphlab.Image constructed from the passed PIL Image
    Parameters
    ----------
        pil_img : PIL.Image
            A PIL Image that is to be converted to a graphlab.Image
    Returns
    --------
        out: graphlab.Image
            The input converted to a graphlab.Image
    """
    # Read in PIL image data and meta data
    height = pil_img.size[1]
    width = pil_img.size[0]
    if pil_img.mode == 'L':
        image_data = bytearray([z for z in pil_img.getdata()])
        channels = 1
    elif pil_img.mode == 'RGB':
        image_data = bytearray([z for l in pil_img.getdata() for z in l ])
        channels = 3
    else:
        image_data = bytearray([z for l in pil_img.getdata() for z in l])
        channels = 4
    format_enum = _format['RAW']
    image_data_size = len(image_data)

    # Construct a graphlab.Image

    img = gl.Image(_image_data=image_data, _width=width, _height=height, _channels=channels, _format_enum=format_enum, _image_data_size=image_data_size)
    return img

def to_pil_image(gl_img):
    """
    Returns a PIL Image constructed from the passed graphlab.Image
    Parameters
    ----------
        gl_img : graphlab.Image
            A graphlab.Image that is to be converted to a PIL Image
    Returns
    -------
        out : PIL.Image
            The input converted to a PIL Image
    """
    if gl_img._format_enum == _format['RAW']:
        # Read in Image, switch based on number of channels.
        if gl_img.channels == 1:
            img = PIL.Image.frombytes('L', (gl_img._width, gl_img._height), str(gl_img._image_data))
        elif gl_img.channels == 3:
            img = PIL.Image.frombytes('RGB', (gl_img._width, gl_img._height), str(gl_img._image_data))
        elif gl_img.channels == 4:
            img = PIL.Image.frombytes('RGBA', (gl_img._width, gl_img._height), str(gl_img._image_data))
        else:
            raise ValueError('Unsupported channel size: ' + str(gl_img.channels))
    else:
        img = PIL.Image.open(_StringIO.StringIO(gl_img._image_data))
    return img