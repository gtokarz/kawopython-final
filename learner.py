import cv2,sys,graphlab as gl,os.path
sys.path.append('modules')
import numpy as np
from Tkinter import *
import ttk,copy
from PIL import Image
from pil_utils import *

dnn = gl.load_model('nn_classifiers/dnn.gl')

person_classifier = gl.load_model('logistic_classifiers/person.gl')
face_classifier = gl.load_model('logistic_classifiers/face.gl')
coffetype_classifier = gl.load_model('logistic_classifiers/coffe_type.gl')
coffe_classifier = gl.load_model('logistic_classifiers/coffe.gl')

def retrain_all():
    global person_classifier,face_classifier,coffetype_classifier,coffe_classifier

    recorder = ['OTHER','NONE','GT_test','MS_new']
    #bazowa sciezka do rekordera
    recorder_base_path = 'coffe_logger_data/recorder/'
    coffe_train = gl.SFrame({})
    coffe_test = gl.SFrame({})
    for ds_name in recorder:
        dir_name = recorder_base_path+ds_name+'/sf'
        if os.path.isdir(dir_name):
            ds_sf = gl.SFrame(dir_name).random_split(.99)
            coffe_train = coffe_train.append(ds_sf[0])
            coffe_test = coffe_test.append(ds_sf[1])
    perfon_classifier_logistic = gl.logistic_classifier.create(coffe_train,target='person',features=['deep_features'], max_iterations = 500)
    perfon_classifier_logistic.save('logistic_classifiers/person.gl')
    coffe_type_logistic = gl.logistic_classifier.create(coffe_train,target='coffe_type',features=['deep_features'], max_iterations = 500)
    coffe_type_logistic.save('logistic_classifiers/coffe_type.gl')
    coffe_logistic = gl.logistic_classifier.create(coffe_train,target='coffe',features=['deep_features'], max_iterations = 500)
    coffe_logistic.save('logistic_classifiers/coffe.gl')
    face_logistic = gl.logistic_classifier.create(coffe_train,target='face',features=['deep_features'], max_iterations = 500)
    face_logistic.save('logistic_classifiers/face.gl')
    person_classifier = perfon_classifier_logistic
    face_classifier = face_logistic
    coffetype_classifier = coffe_type_logistic
    coffe_classifier = coffe_logistic

video_capture = cv2.VideoCapture(0)

class simpleapp_tk(Tk):
    def __init__(self,parent):
        Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()
    def readCam(self):
        if(video_capture.isOpened()):
            ret, frame = video_capture.read()
            raw_frame = copy.copy(frame)
            if ret==True:
                # frame = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
                #pil_im = Image.fromarray(frame)
                frame = frame[0:480,80:560]

                self.person_frame = gl.SFrame({'image':[gl.image_analysis.resize(from_pil_image(Image.fromarray(cv2.cvtColor(frame,cv2.COLOR_BGR2RGB))),256,256,3)]})
                self.person_frame['deep_features'] = dnn.extract_features(self.person_frame)

                person_class = person_classifier.predict_topk(self.person_frame,k=2).sort(['probability'],False)
                face_class = face_classifier.predict_topk(self.person_frame,k=1)[0:1]
                coffetype_class = coffetype_classifier.predict_topk(self.person_frame).sort(['probability'],False)
                coffe_class = coffe_classifier.predict_topk(self.person_frame,k=1)[0:1]

                cv2.putText(frame,'Face?:'+face_class[0]['class']+':'+"{:.4f}".format(face_class[0]['probability']), (0,int(480-25*0.75)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, 255)
                for i in range(0,len(coffetype_class)):
                    cv2.putText(frame,coffetype_class[i]['class']+':'+"{:.4f}".format(coffetype_class[i]['probability']), (240,int(25*(i+1)*0.75)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, 255)
                cv2.putText(frame,'Coffe?:'+coffe_class[0]['class']+':'+"{:.4f}".format(coffe_class[0]['probability']), (240,int(480-25*0.75)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, 255)
                for i in range(0,2,1):
                    cv2.putText(frame,str(person_class[i]['class'])+':'+"{:.4f}".format(person_class[i]['probability']), (0,int(25*(i+1)*0.75)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, 255)
                #cv2.imshow('Video', frame)

                key = cv2.waitKey(1)
                #cv2.putText(frame,str(counter), (0,int(25*0.75)), cv2.FONT_HERSHEY_SIMPLEX, 0.75, 255)
                cv2.imshow('Video', frame)
        self.after(200,func=lambda : self.readCam())
    def learn(self):
        self.person_frame['face'] = [self.face.get()]
        self.person_frame['person'] = [self.person.get()]
        self.person_frame['coffe'] = [self.coffe.get()]
        #CAFE_AU_LAIT,GRANDE
        self.person_frame['coffe_type'] = [self.coffe_type.get()]
        self.data = self.data.append(self.person_frame)
        print "Learnt!"
    def save_data(self):
        prefix = str(self.prefix.get("1.0",END).replace('\n',''))
        self.data.save('coffe_logger_data/recorder/'+prefix+'/sf')
        print "Saved!"
    def initialize(self):
        self.grid()

        self.prefix = Text(self)
        self.prefix.insert(INSERT,'GT_test')
        self.prefix.grid(column=0,row=0,sticky='EW')
        #print(self.prefix.get("1.0",END))
        prefix = str(self.prefix.get("1.0",END).replace('\n',''))
        if os.path.isdir('coffe_logger_data/recorder/'+prefix+'/sf'):
            self.data = gl.SFrame('coffe_logger_data/recorder/'+prefix+'/sf')
        else:
            self.data = gl.SFrame({})
        self.face = ttk.Combobox(self,values = face_classifier.get('classes'))
        self.face.pack()
        self.face.grid(column=1,row=1)

        self.person = ttk.Combobox(self,values = person_classifier.get('classes'))
        self.person.pack()
        self.person.grid(column=2,row=1)

        self.coffe = ttk.Combobox(self,values = coffe_classifier.get('classes'))
        self.coffe.pack()
        self.coffe.grid(column=1,row=2)

        self.coffe_type = ttk.Combobox(self,values = coffetype_classifier.get('classes'))
        self.coffe_type.pack()
        self.coffe_type.grid(column=2,row=2)

        LearnBtn = Button(self,text=u"Learn!",command = self.learn)
        LearnBtn.grid(column=1,row=0)

        LearnBtn = Button(self,text=u"Save Data Set",command = self.save_data)
        LearnBtn.grid(column=2,row=0)

        LearnBtn = Button(self,text=u"ReTrain",command = retrain_all)
        LearnBtn.grid(column=3,row=0)
        #label = Label(self,
        #                      anchor="w",fg="white",bg="blue")
        #label.grid(column=0,row=1,columnspan=2,sticky='EW')

        self.grid_columnconfigure(0,weight=1)

if __name__ == "__main__":
    app = simpleapp_tk(None)
    app.title('my application')
    app.after(0,func=lambda : app.readCam())
    app.mainloop()